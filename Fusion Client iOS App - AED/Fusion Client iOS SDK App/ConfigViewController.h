//
//  ConfigViewController.h
//  Fusion Client iOS SDK App
//
//

#import <UIKit/UIKit.h>
#import <ACBClientSDK/ACBUC.h>

@interface ConfigViewController : UIViewController <ACBUCDelegate>

@property (weak, nonatomic) IBOutlet UITextField *topicName;
@property (weak, nonatomic) IBOutlet UITextField *server;
@property (weak, nonatomic) IBOutlet UIButton *startStopButton;

@property (atomic) bool started;

-(IBAction)toggleStart:(UIButton*)sender;

+(ACBUC *) getACBUC;
+(NSString *) getTopic;


@end
