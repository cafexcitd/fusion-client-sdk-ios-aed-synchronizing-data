//
//  ConfigViewController.m
//  Fusion Client iOS SDK App
//
//

#import "ConfigViewController.h"

@interface ConfigViewController ()

@end

@implementation ConfigViewController

static ACBUC *acbuc;
static NSString *topic;


- (void)viewDidLoad
{
    [super viewDidLoad];
	self.started = false;
}

+ (ACBUC *) getACBUC
{
    return acbuc;
}

+ (NSString *) getTopic
{
    return topic;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/////////////////////////////////////////////////////
//
// UI methods

-(void) setNotStarted
{
    self.started = false;
    self.topicName.enabled = YES;
    [self.startStopButton setTitle:@"Start" forState:UIControlStateNormal];
    self.view.backgroundColor = [UIColor whiteColor];
}

-(void) toggleStart:(UIButton *)sender
{
    // we'll need the server regardless of what the user clicked
    NSString *server = self.server.text;
    
    if (!self.started) {
        // if we've not started, then do so...
        NSString *server = self.server.text;

        
        // get the session id...
        NSString *urlString = [NSString stringWithFormat:@"http://%@/aed/id.php", server];
        NSURL *url = [NSURL URLWithString:urlString];
        NSString *sessionID = [NSString stringWithContentsOfURL:url encoding:NSASCIIStringEncoding error:nil];
        
        NSLog(@"%@", sessionID);
        
        // now init the with the session id
        acbuc = [ACBUC ucWithConfiguration:sessionID delegate:self];
        [acbuc setNetworkReachable:true];
        [acbuc startSession];
        
        NSLog(@"::Session: %@", sessionID);
    }
    else {
        // to log out: 1. notify the server-side component of your app to DELETE
        // the session on the Fusion Web Gateway, and 2. close the connection down
        // from the client application by sending the ACBUC the stopSession message
        NSString *urlString = [NSString stringWithFormat:@"http://%@/logout.php", server];
        NSURL *url = [NSURL URLWithString:urlString];
        [NSData dataWithContentsOfURL:url];
        [acbuc stopSession];
        
        // since there is no callback for stopSession - go ahead and update UI
        [self setNotStarted];
    }
}

-(IBAction)userDoneEnteringText:(id)sender
{
    NSLog(@"::userDoneEnteringText");
    [sender resignFirstResponder];
}

/////////////////////////////////////////////////////
//
// ACBUCDelegate methods

-(void)ucDidStartSession:(ACBUC *)uc
{
    // fix the topic name
    topic = self.topicName.text;
    self.topicName.enabled = NO;
    
    NSLog(@"::ucDidStartSession");
    self.started = true;
    [self.startStopButton setTitle:@"Stop" forState:UIControlStateNormal];
    self.view.backgroundColor = [UIColor colorWithRed:0.5 green:0.8 blue:0.5 alpha:1.0];
}

-(void)ucDidFailToStartSession:(ACBUC *)uc
{
    NSLog(@"::ucDidFailToStartSession");
}

-(void)ucDidLoseConnection:(ACBUC *)uc
{
    NSLog(@"::ucDidLoseConnection");
    [self setNotStarted];
}

-(void)ucDidReceiveSystemFailure:(ACBUC *)uc
{
    NSLog(@"::ucDidReceiveSystemFailure");
    [self setNotStarted];
}

@end
